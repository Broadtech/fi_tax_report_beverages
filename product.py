# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2014 Sprintit Ltd (<http://sprintit.fi>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv

class product_template(osv.osv):
    # additional field for product.template
    _inherit = 'product.template'
    
    _columns = {
        'customs_taxes_id': fields.many2many('account.tax', 'product_customs_taxes_rel', 'prod_id', 'tax_id', 'Customs Taxes')
        }

product_template()


class account_tax(osv.osv):
    _inherit = 'account.tax'
    
    _columns = {
        'alcohol_include': fields.boolean('Alcohol Included')
        }

account_tax()
