# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright idea/design/program by SME ERP IT Consulting Co., Ltd.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.addons.fi_tax_report_beverages import JasperDataParser
from openerp.addons.jasper_reports import jasper_report
from datetime import datetime
from collections import defaultdict

from openerp.osv import fields, osv

class jasper_beverages_report(JasperDataParser.JasperDataParser):
        
    def __init__(self, cr, uid, ids, data, context):
        super(jasper_beverages_report, self).__init__(cr, uid, ids, data, context)
        self.sheet_names=[]
        
    def generate_data_source(self, cr, uid, ids, data, context):
        return 'xml_records'

    def generate_parameters(self, cr, uid, ids, data, context):
#         if data['report_type']=='xls':
#             return {'IS_IGNORE_PAGINATION':True}
        return {}

    def generate_records(self, cr, uid, ids, data, context):
        result_main = []
        result_sub1 = []
        result_sub2 = []
        result_sub3 = []
        pick_list = []
        picking_type_list = []
        tulli_header = self.pool.get('sprintit.tulli.picking.type')
        stock_move_pool = self.pool.get('stock.move')
        date_from = data['form']['date_from']
        date_to = data['form']['date_to']
        location_ids = data['form']['location_ids']
        location_dest_ids = data['form']['location_dest_ids']
        move_ids = []
        tulli_picking_ids = self.pool.get('sprintit.tulli.picking').search(cr, uid, [], context=context)
        if tulli_picking_ids:
            tulli_picking_types_objs = self.pool.get('sprintit.tulli.picking').browse(cr, uid, tulli_picking_ids, context=context)
            for tulli_picking_types in tulli_picking_types_objs:
                for picking_type in tulli_picking_types.picking_type_ids:
                    picking_type_list.append(picking_type.id)
        
        if location_ids and location_dest_ids:
            move_ids = stock_move_pool.search(cr, uid, [('state', '=', 'done'), ('date', '>=', date_from), ('date', '<=', date_to),
                            ('location_id', 'in', location_ids), ('location_dest_id', 'in', location_dest_ids),
                            ('picking_type_id', 'in', picking_type_list)], context=context)
        elif location_ids:
            move_ids = stock_move_pool.search(cr, uid, [('state', '=', 'done'), ('date', '>=', date_from), ('date', '<=', date_to),
                            ('location_id', 'in', location_ids), ('picking_type_id', 'in', picking_type_list)], context=context)
        elif location_dest_ids:
            move_ids = stock_move_pool.search(cr, uid, [('state', '=', 'done'), ('date', '>=', date_from), ('date', '<=', date_to),
                            ('location_dest_id', 'in', location_dest_ids), ('picking_type_id', 'in', picking_type_list)], context=context)
        else:
            move_ids = stock_move_pool.search(cr, uid, [('state', '=', 'done'), ('date', '>=', date_from), 
                                                ('date', '<=', date_to), ('picking_type_id', 'in', picking_type_list)], context=context)
        vals = []
        total = 0
        for move in stock_move_pool.browse(cr, uid, move_ids, context=context):
            flag = True
            picking_ids = tulli_header.search(cr, uid, [], context=context)
            if picking_ids:
                picking_types = tulli_header.browse(cr, uid, picking_ids)
                for picking_type in picking_types:
                    if picking_type.picking_type_ids:
                        for picking_type_id in picking_type.picking_type_ids:
                            if move.picking_type_id and picking_type_id == move.picking_type_id:
                                pick = {
                                    'name': move.picking_type_id.name,
                                    'qty': move.product_uom_qty * move.product_id.x_package_size * move.product_uom.factor_inv
                                    }
                                pick_list.append(pick)
                                flag = False
                    if picking_type.x_package_type:
                        if move.product_id.x_recycling_type == picking_type.x_package_type:
                            pick = {
                                'name': picking_type.x_package_type,
                                'qty': move.product_uom_qty * move.product_id.x_package_size * move.product_uom.factor_inv
                                }
                            pick_list.append(pick)
                            flag = False
                    if picking_type.x_recycle_type:
                        if move.product_id.x_recycle_type == picking_type.x_recycle_type:
                            pick = {
                                'name': picking_type.x_recycle_type,
                                'qty': move.product_uom_qty * move.product_id.x_package_size * move.product_uom.factor_inv
                                }
                            pick_list.append(pick)
                            flag = False
                        
            if move.product_id and move.product_id.customs_taxes_id:
                for customs_taxes_id in move.product_id.customs_taxes_id:
                    qty = move.product_uom_qty * move.product_id.x_package_size * move.product_uom.factor_inv
                    account_taxes = self.pool.get('account.tax')._compute(cr, uid, [customs_taxes_id], qty, 1, product=move.product_id, partner=False, precision=None)
                    account_tax_amount = 0.0
                    for account_tax in account_taxes:
                        account_tax_amount = account_tax.get('amount', 0.0)
                    tax_amount = qty * account_tax_amount
                    if customs_taxes_id.alcohol_include:
                        qty = qty * move.product_id.x_alcohol_percentage
                        tax_amount = (qty * account_tax_amount)/100
                    total = total+tax_amount
                    unit = 'Litre'
                    if customs_taxes_id.name == 'Beverage tax':
                        unit = 'eacl'
                    if customs_taxes_id.name == 'Cider tax class A' or customs_taxes_id.name == 'Cider tax class B':
                        tax_amount = tax_amount/100
                    if move.product_id.intrastat_id:
                        intrastat_name = move.product_id.intrastat_id.name[:4]
                        values = {
                            'intrastat': intrastat_name,
                            'qty': qty,
                            'unit': unit,
                            'tax': customs_taxes_id.name,
                            'tax_id': customs_taxes_id.id,
                            'tax_amount': tax_amount,
                            }
                        vals.append(values)
                    else:
                        values = {
                            'intrastat': 'Uncategorized',
                            'qty': qty,
                            'unit': unit,
                            'tax': customs_taxes_id.name,
                            'tax_id': customs_taxes_id.id,
                            'tax_amount': tax_amount,
                            }
                        vals.append(values)
                    
            values2 = {
                'id': move.id,
                'pick_id': move.picking_id and move.picking_id.name or '',
                'group_id': move.group_id and move.group_id.name or '',
                'product_id': move.product_id and move.product_id.name or '',
                'qty': move.product_uom_qty,
                'product_uom': move.product_uom and move.product_uom.name or '',
                'date': move.date,
                'date_expected': move.create_date,
                'state': move.state
                }
            result_sub3.append(values2) 
            result_sub3 = sorted(result_sub3, key=lambda d: d.get('id'))
            
        if pick_list:
            c = defaultdict(int)
            for d in pick_list:
                c[d['name']] += d['qty']
            pick_list1 = [{'name': name, 'qty': qty} for name, qty in c.items()]
            for name, qty in c.items():
                flag = True
                picking_ids = tulli_header.search(cr, uid, [], context=context)
                if picking_ids:
                    picking_types = tulli_header.browse(cr, uid, picking_ids)
                    for picking_type in picking_types:
                        if picking_type.picking_type_ids:
                            for picking_type_id in picking_type.picking_type_ids:
                                if picking_type_id.name == name:
                                    values1 = {
                                        'name': picking_type.name,
                                        'qty': qty,
                                        'pick_type': 'Picking Type: ' + name
                                        }
                                    result_sub2.append(values1)
                                    flag = False
                        if picking_type.x_package_type and picking_type.x_package_type == name:
                            values1 = {
                                'name': picking_type.name,
                                'qty': qty,
                                'pick_type': 'Package Type: ' + name
                                }
                            result_sub2.append(values1)
                            flag = False
                        if picking_type.x_recycle_type and picking_type.x_recycle_type == name:
                            values1 = {
                                'name': picking_type.name,
                                'qty': qty,
                                'pick_type': 'Recycling Type: ' + name
                                }
                            result_sub2.append(values1)
                            flag = False

                            
            result_sub2 = sorted(result_sub2, key=lambda d: d.get('name'))
            
        vals = sorted(vals, key=lambda d: d.get('tax'))
        vals = sorted(vals, key=lambda d: d.get('intrastat'))
        sheets = ['Beverages_Report','Audit_Trail']
        self.sheet_names.append('Beverages_Report')
        self.sheet_names.append('Audit_Trail')  
        for sheet in sheets:
            result = []
            if sheet=='Beverages_Report':
                if vals:
                    data = {
                        'intrastat': '',
                        'qty': 0.0,
                        'tax': '',
                        'tax_amount': 0.0,
                        'total_tax_amount': total,
                        }
                    for val in vals:
                        if data['intrastat'] and val['intrastat'] == data['intrastat'] and val['tax'] == data['tax']:
                            data['qty'] = data['qty'] + val['qty'] or 0.0
                            data['tax_amount'] = data['tax_amount'] + val['tax_amount'] or 0.0
                        else:
                            if data['intrastat'] and data['qty']:
                                result_sub1.append(data)
                            data = {
                                'intrastat': val['intrastat'],
                                'qty': val['qty'] or 0.0,
                                'unit': val['unit'],
                                'tax': val['tax'] or 0.0,
                                'tax_amount': val['tax_amount'] or 0.0,
                                'total_tax_amount': total or 0.0,
                                }
                    if data['qty']:
                        result_sub1.append(data)
                if not result_sub1:
                    data = {
                        'intrastat': '',
                        'qty': 0.0,
                        'unit': '',
                        'tax': '',
                        'tax_amount': 0.0,
                        'total_tax_amount': 0.0,
                        }
                    result_sub1 = [data]
                data1 = {
                    'subreport_intrastat_field': result_sub1,
                    'subreport_othermove_field': result_sub2
                    }
                result.append(data1)
            elif sheet=='Audit_Trail':
                result = result_sub3
            if result:
                res = {
                    'name': sheet,
                    'details': result,
                    }
                result_main.append(res)
        return result_main
    
    def generate_properties(self, cr, uid, ids, data, context):
        return {
            'net.sf.jasperreports.export.xls.one.page.per.sheet':'true',
            'net.sf.jasperreports.export.xls.sheet.names.all': '/'.join(self.sheet_names),
            'net.sf.jasperreports.export.ignore.page.margins':'true',
            'net.sf.jasperreports.export.xls.remove.empty.space.between.rows':'true',
            'net.sf.jasperreports.export.xls.remove.empty.space.between.columns': 'true',
            'net.sf.jasperreports.export.xls.detect.cell.type': 'true',
            'net.sf.jasperreports.export.xls.white.page.background': 'false',
            'net.sf.jasperreports.export.xls.show.gridlines': 'true',
            'net.sf.jasperreports.export.xls.column.width': '10',
            'net.sf.jasperreports.export.xls.column.width.ratio': '1.0',
            }
    
jasper_report.report_jasper('report.jasper_beverages_report', 'beverages.report', parser=jasper_beverages_report)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: