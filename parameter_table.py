# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################
from openerp.osv import fields,osv

class sprintit_tulli_picking(osv.osv):
    _name = "sprintit.tulli.picking"
    _descritpion = 'Tulli Picking Type'
    _columns = {
        'picking_type_ids': fields.many2many('stock.picking.type', 'tulli_picking_rel', 
                                             'tulli_picking_id', 'picking_type_id', 'Picking Type'),
        }
    
sprintit_tulli_picking()


class sprintit_tulli_picking_type(osv.osv):
    _name = "sprintit.tulli.picking.type"
    _descritpion = 'Tulli Picking Type Header'
    _columns = {
        'name': fields.char('Header', size=32),
        'based_on': fields.selection([('picking', 'Picking Type'),
                                   ('package', 'Package Type'),
                                   ('recycling', 'Recycling Type'),], 'Based On'),
        'x_package_type': fields.selection([('bottle','Bottle'), 
                                              ('can','Can'), 
                                              ('carton','Carton'), 
                                              ('keg','Keg'), 
                                              ('canister','Canister'), 
                                              ('plastic_bottle','Plastic Bottle'), 
                                              ('barrel','Barrel'), 
                                              ('bulk','Bulk'), 
                                              ('bib','BiB'), 
                                              ('other','Other')], 'Package Type'),
        'x_recycle_type': fields.selection([('refill','Refill'), 
                                            ('recycle','Recycle'), 
                                            ('disposable','Disposable'), 
                                            ('no_package','No Package')], 'Recycling Type'),       
        'picking_type_ids': fields.many2many('stock.picking.type', 'tulli_picking_type_rel', 
                                             'tulli_picking_id', 'picking_type_id', 'Picking Type'),
        }
    
sprintit_tulli_picking_type()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: