# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################
from datetime import datetime, date, timedelta

from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from openerp.osv import fields, osv
from openerp.tools.translate import _

class beverages_report(osv.osv_memory):
    _name = 'beverages.report'
    _description = 'Tulli Tax Reporting'
    
    def _date_from(self, cr, uid, context=None):
        date_from = (date.today().replace(day=1) - timedelta(days=1)).replace(day=1)
        return date_from.strftime(DEFAULT_SERVER_DATE_FORMAT)

    def _date_to(self, cr, uid, context=None):
        date_to = date.today().replace(day=1) - timedelta(days=1)
        return date_to.strftime(DEFAULT_SERVER_DATE_FORMAT)
    
    _columns = {
        'date_from': fields.date('Date From'),
        'date_to': fields.date('Date To'),
        'location_ids': fields.many2many('stock.location', 'beverages_report_source_location_rel',
                                          'report_id', 'location_id', 'Source Location'),
        'location_dest_ids': fields.many2many('stock.location', 'beverages_report_destination_location_rel', 
                                          'report_id', 'location_id', 'Destination Location')
        }
    
    _defaults = {
        'date_from': _date_from,
        'date_to': _date_to
        }
    
    def action_print(self, cr, uid, ids, context=None):
        data = {}
        tulli_picking_ids = self.pool.get('sprintit.tulli.picking').search(cr, uid, [], context=context)
        if not tulli_picking_ids:
            raise osv.except_osv(_('Warning!'),_('Add Picking type in Settings->Configuration->MBO->MBO Tulli Picking Types'))
        
        data['ids'] = ids
        data['model'] = 'beverages.report'
        data['form'] = self.read(cr, uid, ids[0], context=context)
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'jasper_beverages_report',
            'report_type': 'xls',
            'datas': data,
            'nodestroy' : True
        }

beverages_report()
