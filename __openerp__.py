# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################
{
    'name': 'Tax Report - Bewerages and sweets',
    'version': '1.92',
    'license': 'Other proprietary',
    'category': 'Settings',
    'description': '''Tax Report - Bewerages and sweets
    ''',
    'author': 'SprintIT',
    'maintainer': 'SprintIT',
    'website': 'http://www.sprintit.fi',
    'depends': [
        'jasper_reports', 'purchase', 'stock', 'report_intrastat', 'sprintit_mbo_customization'
    ],
    'data': [
        'security/ir.model.access.csv',
        'view/product_view.xml',
        'data.xml',
        'parameter_table_view.xml',
        'wizard/beverages_report_view.xml',
        'report.xml'
    ],
    'demo': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
 }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: